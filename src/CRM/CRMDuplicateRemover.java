/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CRM;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.util.Set;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class CRMDuplicateRemover {    
    
    public static void main(String[] args){    
        System.out.println("Enter a filepath for input and output files");
        Scanner in = new Scanner(System.in);
        
        File inFile  = new File(in.next());
        File outFile = new File(in.next());
        
        try{
            removeDuplicates(inFile, outFile);
        }
        catch (IOException e) {
            System.err.println(e);
            System.exit(1);        
        }
        
    }
    
    public static void removeDuplicates(File fromFile, File toFile) throws IOException {
        String[] cdataArray;
        String line = null;        
        int counter = 0;
        
        BufferedReader br = new BufferedReader(new FileReader(fromFile));
        BufferedWriter bw = new BufferedWriter(new FileWriter(toFile));
        Set<CRMData> cdataSet = new LinkedHashSet<CRMData>();
        while ((line = br.readLine()) != null) {
        	counter++;
            cdataArray = line.split("\t");
            CRMData cdata = new CRMData(cdataArray[25], cdataArray[12], cdataArray[17]);
            if (!cdataSet.contains(cdata)) {
                cdataSet.add(cdata);
                //System.out.println(cdata.toString());
                bw.write(line);
                bw.newLine();
            }
        }
        System.out.println("Set size before removal of duplicates: " + counter);
        System.out.println("Set size after removal of duplicates: " + cdataSet.size());
        
        if (br != null) {
            br.close();
        }
        if (bw != null) {
            bw.close();
        }  
    }      
} 