/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package CRM;

public class CRMData {

	private String emailId;
    private String createDate;
    private String optinDate;        

	public CRMData(String emailId, String createDate, String optinDate) {
        this.createDate = createDate;
        this.optinDate = optinDate;
		this.emailId = emailId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof CRMData)) {
			return false;
		}
		CRMData other = (CRMData) obj;
		if (createDate == null) {
			if (other.optinDate != null) {
				return false;
			}
		} else if (!createDate.equals(other.optinDate)) {
			return false;
		}
		if (emailId == null) {
			if (other.emailId != null) {
				return false;
			}
		} else if (!emailId.equals(other.emailId)) {
			return false;
		}
		if (optinDate == null) {
			if (other.createDate != null) {
				return false;
			}
		} else if (!optinDate.equals(other.createDate)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((emailId == null) ? 0 : emailId.hashCode());
		result = prime * result
				+ ((optinDate == null) ? 0 : optinDate.hashCode());
		return result;
	}
        
    @Override
    public String toString() {
    	StringBuilder result = new StringBuilder();
        String NEW_LINE = System.getProperty("line.separator");
            
        result.append("\t");
        result.append(emailId);
        result.append("\t");
        result.append(createDate);
        result.append("\t");
        result.append(optinDate);
        result.append("\t");
        result.append(NEW_LINE);
        return result.toString();
	}
}